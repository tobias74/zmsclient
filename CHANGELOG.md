## v2.24.01

* #49629 Sicherheit: Aktualisierung zentraler Bibliotheken für Stabilität und Sicherheit des Systems durchgeführt
* #52271 Bugfix: Die Authentifizierung für einen Request kann nun konfiguriert werden
## v2.23.04

* #42054 Bugfix: Die Standort-ID wird jetzt bei Standortwechsel korrekt ausgelesen.
* #35372 Bugfix: Der Parameter gql kann jetzt auch mit Listen verwendet werden.

## v2.22.00

* #36425 HTTPS ist bei Cookies im Ticketprinter keine Pflicht mehr

## v2.20.01

* #36528 Refactoring: Gemeinsame Funktionen zum Holen der Warteliste vom Sachbearbeiter und Ticketprinter hierher verschoben

## v2.20.00

* #36102 Funktionen für Apikey und Workflowkey ergänzt, um die Public API besser testen zu können
* #35013 SessionHandler angepasst für PHP-Update

## v2.19.05

* #35764 Deploy Tokens eingebaut

## v2.19.02

## v2.19.01

## v2.19.00

* Ticketprinter: Behalte Cookie für 10 Jahre

## v2.18.02

* Bugfix zu Cookie-Pfaden (betrifft nur ticketprinter)


## v2.18.00

* #34272 Flag um im SessionHandler den Parameter "sync" zu verwenden
